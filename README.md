# Django MSSQL Multi Tenant

Django MSSQL Multi Tenant is a generic abstration to use a multi tenant architecture on Django application
using multipiple MSSQL Databases, based on url subdomain.

```
Tenant 1: tenant1.sample.com
Tenant 2: tenant2.sample.com
```

This application solve a multi tenant settings too.

## Setup

Can be installed from PyPI with tools like pip:

```shell
pip install git+https://gitlab.com/r13/educat-community/django-mssql-multitenant
```

Add 'tenants' to your INSTALLED_APPS.

```python
INSTALLED_APPS = [
    ...
    'tenants',
]
```

Add a middleware class to listen in on responses.

```python
MIDDLEWARE = [
    ...,
    'django.middleware.csrf.CsrfViewMiddleware',
    'tenants.middlewares.TenantMiddleware',
    ...,
]
```

Create a tenants file, to configure your tenants variables, by default a tenant file is called `tenants.json`

```json
[
  {
    "name": "default",
    "host": "multitenant1",
    "db_url": "mssql://user:password@host:port/database"
  },
  {
    "name": "multitenant2",
    "host": "multitenant2",
    "db_url": "mssql://user:password@host:port/database"
  }
]
```

This application use a attribute `name` to create a database configuration therefore Django requires a database using a default name. Then currently one tenant requires a default as name.

## Running migrations

To run a migration should use a basic django migration with database arg

```python
python manage.py migrate --database <tenant name>
```

Or use a django-mssql-multitenant migration

```python
python manage.py migrate_all [--num_threads NUM_THREADS]
```

Should use NUM_THREADS to run a migration command in parallel, default is 3.

## Add more variables to tenant

This application supports add more variavles to a tenant, for this is necessary override a base tenant schema.

Then, will need create a method to get a new schema:

```python
def get_tenant_schema_handler():
    return Tenant
```

Override a base Tenant Schema:

```python
from dataclasses import dataclass
from tenants.schemas import BaseTenant


@dataclass
class Tenant(BaseTenant):
    var1: str
    var2: str
    var3: str
```

Add configuration on settings.py

```python
TENANTS = {
    'GET_SCHEMA_HANDLER': 'path.to.get_tenant_schema_handler',
}
```

Add variables to tenants.json

```json
[
  ...
  {
    "name": "multitenant2",
    "host": "multitenant2",
    "db_url": "mssql://user:password@host:port/database",
    "var1": "var1",
    "var2": "var2",
    "var3": "var3"
  },
  ...
]
```

To access this variable just use a current tenant helper

```python
from tenants.current import tenant

print(tenant.var1)
```

## Caveats

In the currenty application version to find a tenant is required a request, therefore is not work on commands, cronjobs, etc...
