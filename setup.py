import setuptools

with open("README.md", "r", encoding="utf-8") as fh:
    long_description = fh.read()

setuptools.setup(
    name="django-mssql-multitenant",
    version="0.0.1",
    author="Lucas Corrêa",
    author_email="lucas@educat.net.br",
    description="Add support a multi tenant with SQL Server and Django Settings",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/r13/educat-community/django-mssql-multitenant",
    classifiers=[
        "Environment :: Web Environment",
        "Framework :: Django",
        "Intended Audience :: Developers",
        "License :: OSI Approved :: BSD License",
        "Operating System :: OS Independent",
        "Programming Language :: Python",
        "Programming Language :: Python :: 3",
        "Programming Language :: Python :: 3 :: Only",
        "Programming Language :: Python :: 3.6",
        "Programming Language :: Python :: 3.7",
        "Programming Language :: Python :: 3.8",
        "Topic :: Internet :: WWW/HTTP",
        "Topic :: Internet :: WWW/HTTP :: Dynamic Content",
    ],
    packages=setuptools.find_packages(),
    python_requires=">=3.6",
)
