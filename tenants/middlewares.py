import threading

from django.template import exceptions

from tenants.utils import (
    get_tenant_by_name,
    tenant_db_from_request,
)

THREAD_LOCAL = threading.local()


class TenantMiddleware:
    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        db = tenant_db_from_request(request)
        setattr(THREAD_LOCAL, "DB", db)
        response = self.get_response(request)
        return response

def get_current_db_name():
    return getattr(THREAD_LOCAL, "DB", None)

def set_db_for_router(tenant_name: str):
    setattr(THREAD_LOCAL, "DB", tenant_name)


def get_current_tenant(force_tenant_name=None):
    tenant_name = getattr(THREAD_LOCAL, "DB", None)

    if not tenant_name and not force_tenant_name:
        raise exceptions.NoActiveTenant()

    return get_tenant_by_name(tenant_name or force_tenant_name)
