class InvalidHostError(Exception):
    def __init__(self, host: str):
        msg = f'Invalid host "{host}". Please provide a valid host according to your tenants file.'
        super().__init__(msg)


class InvalidTenantError(Exception):
    def __init__(self):
        msg = f'Invalid tenant. Please check your tenants file to ensure it meets the requirements.'
        super().__init__(msg)


class InvalidTenantsFile(Exception):
    def __init__(self):
        msg = f'Your tenants file should be a valid JSON.'
        super().__init__(msg)


class TenantsFileNotFound(Exception):
    def __init__(self, filename: str):
        msg = f'Your tenants file "{filename}" was not found in the system.'
        super().__init__(msg)


class InvalidDbUrl(Exception):
    def __init__(self, db_url: str):
        msg = f'Your db url "{db_url}" should be in the format "engine://user@pwd:host:port/db".'
        super().__init__(msg)


class NoActiveTenant(Exception):
    def __init__(self):
        msg = f'No active tenant founded, force a tenant if is not a request/response process'
        super().__init__(msg)
