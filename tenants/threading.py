import threading

class MultiThreading:

    def __init__(self, thread_function, thread_args, num_threads=3) -> None:
        self.thread_function = thread_function
        self.num_threads = num_threads
        self.thread_args = thread_args
        self.threads = []

    def create_threads(self, thread_args):
        self.threads = []
        for thread_arg in thread_args:
            x = threading.Thread(target=self.thread_function, args=[thread_arg])
            self.threads.append(x)

    def start_threads(self):
        for t in self.threads:
            t.start()

    def join_threads(self):
        for t in self.threads:
            t.join()

    def split_thread_args(self, thread_args, group_size):
        splited_thread_args = []

        for i in range(0, len(thread_args), group_size):
            splited_thread_args.append(thread_args[i:i + group_size])

        return splited_thread_args

    def run(self):
        splited_thread_args = self.split_thread_args(self.thread_args, self.num_threads)

        for thread_args in splited_thread_args:
            self.create_threads(thread_args)
            self.start_threads()
            self.join_threads()
