from django.conf import settings


DEFAULT_TENANT_SETTINGS = {
    'FILE_PATH': 'tenants.json',
    'GET_SCHEMA_HANDLER': 'tenants.schemas.get_tenant_schema_handler'
}


def get_tenant_settings():
    project_tenants_settings = getattr(settings, 'TENANTS', {})

    return {
        setting_name: project_tenants_settings.get(setting_name, setting_value)
        for setting_name, setting_value in DEFAULT_TENANT_SETTINGS.items()
    }
