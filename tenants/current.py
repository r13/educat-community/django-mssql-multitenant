from tenants.middlewares import get_current_tenant
from tenants.schemas import get_tenant_schema


class CurrentTenant:

    @classmethod
    def get_schema_fields(cls):
        TenantSchema = get_tenant_schema()
        return list(TenantSchema.__dataclass_fields__.keys())


    @classmethod
    def create_property(cls, name):
        def __base_property(self):
            current_tenant = get_current_tenant()
            return getattr(current_tenant, name)

        return property(__base_property)

    @classmethod
    def __init__(cls) -> None:
        schema_fields = cls.get_schema_fields()
        for schema_field in schema_fields:
            setattr(cls, schema_field, cls.create_property(schema_field))

tenant = CurrentTenant()
