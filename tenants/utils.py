import importlib
import json
import threading


from django.http import HttpRequest

from tenants import exceptions


from tenants.settings import get_tenant_settings
from functools import lru_cache


@lru_cache
def get_tenants():
    from tenants.schemas import get_tenant_schema
    Tenant = get_tenant_schema()

    filename = get_tenant_settings()['FILE_PATH']

    try:
        with open(filename) as f:
            return [Tenant(**t) for t in json.loads(f.read())]
    except FileNotFoundError as exc:
        raise exceptions.TenantsFileNotFound(filename) from exc
    except json.decoder.JSONDecodeError as exc:
        raise exceptions.InvalidTenantsFile from exc
    except TypeError as exc:
        raise exceptions.InvalidTenantError from exc


def get_tenant_per_host():
    return {t.host: t for t in get_tenants()}

def hostname_from_request(request: HttpRequest) -> str:
    return request.get_host().split(".")[0].split(':')[0].lower()

def is_test_host(host: str):
    return host == 'testserver'

def tenant_db_from_request(request: HttpRequest) -> str:
    host = hostname_from_request(request)
    if is_test_host(host):
        return get_tenant_per_host()['localhost'].name
    try:
        return get_tenant_per_host()[host].name
    except KeyError as exc:
        raise exceptions.InvalidHostError(host) from exc

def get_database_settings():
    return {t.name: t.db_config.to_django() for t in get_tenants()}

def get_method_from_path(method_path):
    module_name, method_name = method_path.rsplit('.', 1)
    module = importlib.import_module(module_name)
    return getattr(module, method_name)


def get_tenant_by_name(tenant_name):
    for tenant in get_tenants():
        if tenant.name == tenant_name:
            return tenant

    raise exceptions.InvalidTenantError()
