from dataclasses import dataclass
import re
import os
from tenants import exceptions

from tenants.settings import get_tenant_settings
from tenants.utils import get_method_from_path

@dataclass
class DbConfig:
    engine: str
    user: str
    pwd: str
    host: str
    port: str
    db: str

    def to_django(self):
        UNLIMITED_PERSISTENT_CONNECTIONS = os.getenv('UNLIMITED_PERSISTENT_CONNECTIONS')
        CONN_MAX_AGE = int(os.getenv('DATABASE_CONN_MAX_AGE', '0'))

        if UNLIMITED_PERSISTENT_CONNECTIONS:
            CONN_MAX_AGE = None

        return {
            'ENGINE': 'sql_server.pyodbc',
            'NAME': self.db,
            'HOST': f'tcp:{self.host},{self.port}',
            'PORT': self.port,
            'USER': self.user,
            'PASSWORD': self.pwd,
            'CONN_MAX_AGE': CONN_MAX_AGE,
            'OPTIONS': {
                'driver': 'ODBC Driver 17 for SQL Server',
                'host_is_server': True,
                'autocommit': True,
                'unicode_results': True,
                'extra_params': 'tds_version=7.4',
                'connection_timeout': 20,
                'query_timeout': 20,
            },
        }

@dataclass
class BaseTenant:
    name: str
    db_url: str
    host: str

    @property
    def db_config(self):
        # engine://user@pwd:host:port/db
        pattern = r'(\S+)://(\S+):(\S+)@(\S+):(\S+)/(\S+)'
        try:
            groups = re.match(pattern, self.db_url).groups()
        except AttributeError as exc:
            raise exceptions.InvalidDbUrl(self.db_url) from exc
        return DbConfig(*groups)


def get_tenant_schema_handler():
    return BaseTenant

def get_tenant_schema():
    get_schema_handler_path = get_tenant_settings()['GET_SCHEMA_HANDLER']
    get_tenant_schema_method = get_method_from_path(get_schema_handler_path)
    return get_tenant_schema_method()
