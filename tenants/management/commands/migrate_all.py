from django.core.management import call_command
from django.core.management.base import BaseCommand
from tenants.schemas import get_tenant_schema
from tenants.threading import MultiThreading

from tenants.utils import get_tenants

Tenant = get_tenant_schema()


class Command(BaseCommand):

    def add_arguments(self, parser):
        parser.add_argument(
            '--num_threads',
            default=3,
            type=int,
            required=False,
            help='Number of threads to run command in parallel'
        )

    def migrate(self, tenant: Tenant):
        print(f'starting migrate {tenant.name}....')
        call_command('migrate', database=tenant.name)
        print(f'finish migrate {tenant.name}....')

    def handle(self, *args, **options):
        num_threads = options['num_threads']
        mt = MultiThreading(self.migrate, get_tenants(), num_threads=num_threads)
        mt.run()
